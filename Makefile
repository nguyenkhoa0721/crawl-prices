dev: down
	docker-compose -p debion_crawl_backend -f ./docker/docker-compose.dev.yml up -d

######################################################################

down:
	-docker-compose -p debion_crawl_backend -f ./docker/docker-compose.dev.yml down

setup:
	docker run --rm -it --name debion_crawl_builder -v $(CURDIR):/app node:14-alpine /bin/ash -c "cd /app && \
																								yarn install --pure-lockfile"

######################################################################