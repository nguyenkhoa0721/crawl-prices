import { Dialect, Sequelize } from "sequelize";
import ProductModel from "./models/products";
import PriceModel from "./models/prices";

const sequelize = new Sequelize(
  process.env.MYSQL_DATABASE as string,
  process.env.MYSQL_USERNAME as string,
  process.env.MYSQL_PASSWORD as string,
  {
    host: process.env.MYSQL_HOST,
    port: Number(process.env.MYSQL_PORT),
    dialect: process.env.MYSQL_DIALECT as Dialect,
  }
);

sequelize
  .authenticate()
  .then(() => {
    console.log("Connection has been established successfully.");
  })
  .catch((err: any) => {
    console.error("Unable to connect to the database:", err);
  });

const db = {
  sequelize: sequelize,
  DataType: Sequelize,
  Product: ProductModel(sequelize, Sequelize),
  Price: PriceModel(sequelize, Sequelize),
};

db.Product.hasMany(db.Price, {
  foreignKey: "productId",
  as: "prices",
});

export default db;
