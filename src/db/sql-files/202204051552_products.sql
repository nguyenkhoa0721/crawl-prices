CREATE TABLE debion_crawl.products (
	id INT auto_increment NOT NULL,
	product_name VARCHAR(255) NULL,
	CONSTRAINT products_PK PRIMARY KEY (id)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_unicode_ci;
