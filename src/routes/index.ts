import express from "express";
import { crawl } from "../controllers/product-controller";

const router = express.Router();

router.get("/", crawl);

export default router;
