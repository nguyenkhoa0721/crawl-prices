import db from "../db";
// const { Product, Price } = db;
import puppeteer from "puppeteer-extra";
import StealthPlugin from "puppeteer-extra-plugin-stealth";
import * as fs from "fs";
import { compare } from "imagemagick-compare";
import fetch from "node-fetch";
import crypto from "crypto";

let browser: any;
const launchBrower = async () => {
  puppeteer.use(StealthPlugin());
  browser = await puppeteer.launch({ headless: true });
};

launchBrower();

const compareImage = async (o: string, c: string) => {
  const original = fs.readFileSync(o);
  const compareWith = fs.readFileSync(c);
  const options: any = { metric: "SSIM", radius: 2 };

  const res = await compare(original, compareWith, options);
  return res;
};

export const getProductAWS = async (url: string, sessionId: string) => {
  const page = await browser.newPage();
  await page.goto(url);
  let price, productTitle, image;
  try {
    price = await page.$eval(".apexPriceToPay .a-offscreen", (element: any) => element.innerHTML);
    productTitle = await page.$eval("#productTitle", (element: any) => element.innerHTML);
    image = await page.$eval("#landingImage", (element: any) => element.getAttribute("src"));

    let fimg = await fetch(image as string);
    fimg.body.pipe(fs.createWriteStream(`./assets/${sessionId}/o.jpg`));
    await page.close();
  } catch (error) {
    // Your chance to handle errors
    console.error(error);
  }
  return { title: productTitle.trim(), price: price.trim(), image: image.trim() };
};

const findProductWayfair = async (search: string) => {
  const browser = await puppeteer.launch();
  const page = await browser.newPage();
  await page.setUserAgent(
    "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.75 Safari/537.36"
  );
  await page.setViewport({
    width: 1366,
    height: 768,
  });
  await page.goto(`https://www.wayfair.com/keyword.php?keyword=${search}`);
  const text = await page.$eval(".pl-ProductCardName > span", (element) => element.innerHTML);
  const price = await page.$eval(".SFPrice .pl-Price-V2--3000", (element) => element.innerHTML);
  console.log(text, price);

  await browser.close();
};

export const findProductWalmart = async (search: string, sessionId: string) => {
  const page = await browser.newPage();
  await page.setViewport({ width: 1280, height: 800 });
  await page.goto(`https://www.walmart.com/search?q=${search}`);

  await page.screenshot({ path: "image.png" });
  const imgs = await page.$$eval('[data-testid="productTileImage"]', (imgs: any) =>
    imgs.map((img: any) => img.getAttribute("src"))
  );
  const productTitles = await page.$$eval(
    '[class="f6 f5-l normal dark-gray mb0 mt1 lh-title"]',
    (elements: any) => elements.map((element: any) => element.innerHTML)
  );
  const prices = await page.$$eval(
    '[class="flex flex-wrap justify-start items-center lh-title mb2 mb1-m"] div:first-child',
    (elements: any) => elements.map((element: any) => element.innerHTML)
  );
  const links = await page.$$eval('[class="absolute w-100 h-100 z-1"]', (elements: any) =>
    elements.map((element: any) => element.getAttribute("href"))
  );

  let scores: Array<number> = [];
  let similarityScore: Array<any> = [];

  const searchPreProcess = await preprocess(search);
  for (let i in imgs) {
    if (Number(i) > 10) {
      break;
    }
    let imgUrl = (imgs[i] as string).replace(
      "?odnHeight=580&odnWidth=580&odnBg=FFFFFF",
      "?odnHeight=350&odnWidth=350&odnBg=FFFFFF"
    );
    let fimg = await fetch(imgUrl);
    const buffer = await fimg.buffer();
    fs.writeFileSync(`./assets/${sessionId}/c${i}.jpeg`, buffer);

    const comparePreProcess = await preprocess(productTitles[i]);

    let score = await compareImage(
      `./assets/${sessionId}/o.jpg`,
      `./assets/${sessionId}/c${i}.jpeg`
    );
    const titleScore = await compareTitle(searchPreProcess, comparePreProcess);
    if (score) {
      similarityScore.push({
        image: score,
        title: titleScore,
      });
      score += titleScore;
      scores.push(score);
    } else {
      similarityScore.push({
        image: 0,
        title: titleScore,
      });
    }
  }
  const maxscore = Math.max(...scores);
  const indexMaxscore = scores.indexOf(maxscore);
  await page.close();
  return {
    title: productTitles[indexMaxscore].trim(),
    price: prices[indexMaxscore].trim(),
    image: imgs[indexMaxscore].trim(),
    url: "https://www.walmart.com" + links[indexMaxscore].trim(),
    similarityScore: similarityScore[indexMaxscore],
  };
};

const preprocess = async (input: string) => {
  input = input.toLowerCase();
  input = input.trim();
  input = input.replace(" ", "");
  input = input.replace(/[^a-zA-Z0-9]/g, "");
  return input;
};
const compareTitle = async (o: string, c: string) => {
  c = "*" + c;
  o = "*" + o;
  const score: number[][] = [[0], [0]];
  for (let i = 0; i < o.length; i++) {
    score.push([]);
    for (let j = 0; j < c.length; j++) {
      score[i][j] = 0;
    }
  }
  for (let i = 1; i < o.length; i++) {
    for (let j = 1; j < c.length; j++) {
      if (o.charAt(i) === c.charAt(j)) score[i][j] = score[i - 1][j - 1] + 1;
      else score[i][j] = Math.max(score[i - 1][j], score[i][j - 1]);
    }
  }
  return score[o.length - 1][c.length - 1] / (o.length - 1);
};

export const crawlPrice = async (url: any) => {
  // compareImage();
  const sessionId = crypto.randomUUID();
  fs.mkdirSync(`./assets/${sessionId}/`);
  const aws = await getProductAWS(url, sessionId);
  const walmart = await findProductWalmart(aws.title, sessionId);
  return { aws, walmart };
};
