import express from "express";
import helmet from "helmet";
import cors from "cors";
import morgan from "morgan";
import routes from "./routes";
import tele from "./utils/tele";
//cors
tele;

const whitelist = process.env.ACCESS_CONTROL_ALLOW_ORIGIN!.split(",");
const options: cors.CorsOptions = {
  origin: whitelist,
  credentials: true,
};

const app = express();
app.use(helmet());
app.use(cors(options));
app.use(morgan("tiny"));
app.use(express.json());

app.use("/api/v1", routes);

export default app;
