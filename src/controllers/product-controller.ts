import { crawlPrice } from "../services/product-service";
import { Request, Response } from "express";

export const crawl = async (req: Request, res: Response) => {
  let result = await crawlPrice(req.query.url);
  return res.status(200).send(result);
};
