import { Telegraf } from "telegraf";
import { getProductAWS, findProductWalmart } from "../services/product-service";
import crypto from "crypto";
import * as fs from "fs";

const bot = new Telegraf(process.env.BOT_TOKEN);

function sleep(ms: number) {
  return new Promise((resolve) => setTimeout(resolve, ms));
}

async function sendMsg(ctx: any) {
  const sessionId = crypto.randomUUID();
  try {
    fs.mkdirSync(`./assets/${sessionId}/`);
    ctx.reply("⌛ Wait a minute for crawling from AMAZON");
    const aws = await getProductAWS(ctx.message.text, sessionId);
    ctx.reply(
      `AMAZON:\n✒️ Product name: ${aws.title}\n💵 Price: ${aws.price}\n📷 Image: ${aws.image}`
    );
    ctx.reply("⌛ Wait a minute for finding similar product on WALMART");
    const walmart = await findProductWalmart(aws.title, sessionId);
    fs.rmSync(`./assets/${sessionId}/`, { recursive: true, force: true });
    const similarityScore: any = walmart.similarityScore;
    ctx.reply(
      `WALMART:\n✒️ Product name: ${walmart.title}\n💵 Price: ${walmart.price}\n📷 Image: ${
        walmart.image
      }
---------------
🔎 Similarity score:
title: ${similarityScore.title.toFixed(3)}
image: ${similarityScore.image.toFixed(3)}`,
      {
        reply_markup: {
          inline_keyboard: [[{ text: "Visit", url: walmart.url }]],
        },
      }
    );
  } catch (err) {
    console.log(err);
    fs.rmSync(`./assets/${sessionId}/`, { recursive: true, force: true });
    ctx.reply("Something went wrong ❌");
  }
}

bot.on("text", async (ctx: any) => {
  console.log(ctx.from);
  sendMsg(ctx);
});

bot.launch();

// Enable graceful stop
process.once("SIGINT", () => bot.stop("SIGINT"));
process.once("SIGTERM", () => bot.stop("SIGTERM"));

export default bot;
