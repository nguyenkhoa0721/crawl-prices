FROM node:14-alpine

WORKDIR /app/debion/trade-platform/api

ENTRYPOINT ["yarn", "start"]