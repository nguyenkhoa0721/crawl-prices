"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const ProductModel = (sequelize, DataType) => {
    const Products = sequelize.define("products", {
        id: {
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
            type: DataType.INTEGER,
        },
        productName: {
            type: DataType.STRING,
        },
    }, {
        underscored: true,
        paranoid: true,
        timestamps: true,
        charset: "utf8",
        collate: "utf8_unicode_ci",
    });
    return Products;
};
exports.default = ProductModel;
