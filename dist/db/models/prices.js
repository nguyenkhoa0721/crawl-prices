"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const PriceModel = (sequelize, DataType) => {
    const Prices = sequelize.define("prices", {
        productId: {
            type: DataType.INTEGER,
            references: {
                model: "products",
                key: "id",
            },
        },
        inventory: {
            type: DataType.STRING,
        },
        price: {
            type: DataType.DECIMAL,
        },
    }, {
        underscored: true,
        paranoid: true,
        timestamps: true,
        charset: "utf8",
        collate: "utf8_unicode_ci",
    });
    return Prices;
};
exports.default = PriceModel;
