"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const sequelize_1 = require("sequelize");
const products_1 = __importDefault(require("./models/products"));
const prices_1 = __importDefault(require("./models/prices"));
const sequelize = new sequelize_1.Sequelize(process.env.MYSQL_DATABASE, process.env.MYSQL_USERNAME, process.env.MYSQL_PASSWORD, {
    host: process.env.MYSQL_HOST,
    port: Number(process.env.MYSQL_PORT),
    dialect: process.env.MYSQL_DIALECT,
});
sequelize
    .authenticate()
    .then(() => {
    console.log("Connection has been established successfully.");
})
    .catch((err) => {
    console.error("Unable to connect to the database:", err);
});
const db = {
    sequelize: sequelize,
    DataType: sequelize_1.Sequelize,
    Product: (0, products_1.default)(sequelize, sequelize_1.Sequelize),
    Price: (0, prices_1.default)(sequelize, sequelize_1.Sequelize),
};
db.Product.hasMany(db.Price, {
    foreignKey: "productId",
    as: "prices",
});
exports.default = db;
