"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.crawlPrice = exports.findProductWalmart = exports.getProductAWS = void 0;
// const { Product, Price } = db;
const puppeteer_extra_1 = __importDefault(require("puppeteer-extra"));
const puppeteer_extra_plugin_stealth_1 = __importDefault(require("puppeteer-extra-plugin-stealth"));
const fs = __importStar(require("fs"));
const imagemagick_compare_1 = require("imagemagick-compare");
const node_fetch_1 = __importDefault(require("node-fetch"));
const crypto_1 = __importDefault(require("crypto"));
let browser;
const launchBrower = () => __awaiter(void 0, void 0, void 0, function* () {
    puppeteer_extra_1.default.use((0, puppeteer_extra_plugin_stealth_1.default)());
    browser = yield puppeteer_extra_1.default.launch({ headless: true });
});
launchBrower();
const compareImage = (o, c) => __awaiter(void 0, void 0, void 0, function* () {
    const original = fs.readFileSync(o);
    const compareWith = fs.readFileSync(c);
    const options = { metric: "SSIM", radius: 2 };
    const res = yield (0, imagemagick_compare_1.compare)(original, compareWith, options);
    return res;
});
const getProductAWS = (url, sessionId) => __awaiter(void 0, void 0, void 0, function* () {
    const page = yield browser.newPage();
    yield page.goto(url);
    let price, productTitle, image;
    try {
        price = yield page.$eval(".apexPriceToPay .a-offscreen", (element) => element.innerHTML);
        productTitle = yield page.$eval("#productTitle", (element) => element.innerHTML);
        image = yield page.$eval("#landingImage", (element) => element.getAttribute("src"));
        let fimg = yield (0, node_fetch_1.default)(image);
        fimg.body.pipe(fs.createWriteStream(`./assets/${sessionId}/o.jpg`));
        yield page.close();
    }
    catch (error) {
        // Your chance to handle errors
        console.error(error);
    }
    return { title: productTitle.trim(), price: price.trim(), image: image.trim() };
});
exports.getProductAWS = getProductAWS;
const findProductWayfair = (search) => __awaiter(void 0, void 0, void 0, function* () {
    const browser = yield puppeteer_extra_1.default.launch();
    const page = yield browser.newPage();
    yield page.setUserAgent("Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.75 Safari/537.36");
    yield page.setViewport({
        width: 1366,
        height: 768,
    });
    yield page.goto(`https://www.wayfair.com/keyword.php?keyword=${search}`);
    const text = yield page.$eval(".pl-ProductCardName > span", (element) => element.innerHTML);
    const price = yield page.$eval(".SFPrice .pl-Price-V2--3000", (element) => element.innerHTML);
    console.log(text, price);
    yield browser.close();
});
const findProductWalmart = (search, sessionId) => __awaiter(void 0, void 0, void 0, function* () {
    const page = yield browser.newPage();
    yield page.setViewport({ width: 1280, height: 800 });
    yield page.goto(`https://www.walmart.com/search?q=${search}`);
    yield page.screenshot({ path: "image.png" });
    const imgs = yield page.$$eval('[data-testid="productTileImage"]', (imgs) => imgs.map((img) => img.getAttribute("src")));
    const productTitles = yield page.$$eval('[class="f6 f5-l normal dark-gray mb0 mt1 lh-title"]', (elements) => elements.map((element) => element.innerHTML));
    const prices = yield page.$$eval('[class="flex flex-wrap justify-start items-center lh-title mb2 mb1-m"] div:first-child', (elements) => elements.map((element) => element.innerHTML));
    const links = yield page.$$eval('[class="absolute w-100 h-100 z-1"]', (elements) => elements.map((element) => element.getAttribute("href")));
    let scores = [];
    let similarityScore = [];
    const searchPreProcess = yield preprocess(search);
    for (let i in imgs) {
        if (Number(i) > 10) {
            break;
        }
        let imgUrl = imgs[i].replace("?odnHeight=580&odnWidth=580&odnBg=FFFFFF", "?odnHeight=350&odnWidth=350&odnBg=FFFFFF");
        let fimg = yield (0, node_fetch_1.default)(imgUrl);
        const buffer = yield fimg.buffer();
        fs.writeFileSync(`./assets/${sessionId}/c${i}.jpeg`, buffer);
        const comparePreProcess = yield preprocess(productTitles[i]);
        let score = yield compareImage(`./assets/${sessionId}/o.jpg`, `./assets/${sessionId}/c${i}.jpeg`);
        const titleScore = yield compareTitle(searchPreProcess, comparePreProcess);
        if (score) {
            similarityScore.push({
                image: score,
                title: titleScore,
            });
            score += titleScore;
            scores.push(score);
        }
        else {
            similarityScore.push({
                image: 0,
                title: titleScore,
            });
        }
    }
    const maxscore = Math.max(...scores);
    const indexMaxscore = scores.indexOf(maxscore);
    yield page.close();
    return {
        title: productTitles[indexMaxscore].trim(),
        price: prices[indexMaxscore].trim(),
        image: imgs[indexMaxscore].trim(),
        url: "https://www.walmart.com" + links[indexMaxscore].trim(),
        similarityScore: similarityScore[indexMaxscore],
    };
});
exports.findProductWalmart = findProductWalmart;
const preprocess = (input) => __awaiter(void 0, void 0, void 0, function* () {
    input = input.toLowerCase();
    input = input.trim();
    input = input.replace(" ", "");
    input = input.replace(/[^a-zA-Z0-9]/g, "");
    return input;
});
const compareTitle = (o, c) => __awaiter(void 0, void 0, void 0, function* () {
    c = "*" + c;
    o = "*" + o;
    const score = [[0], [0]];
    for (let i = 0; i < o.length; i++) {
        score.push([]);
        for (let j = 0; j < c.length; j++) {
            score[i][j] = 0;
        }
    }
    for (let i = 1; i < o.length; i++) {
        for (let j = 1; j < c.length; j++) {
            if (o.charAt(i) === c.charAt(j))
                score[i][j] = score[i - 1][j - 1] + 1;
            else
                score[i][j] = Math.max(score[i - 1][j], score[i][j - 1]);
        }
    }
    return score[o.length - 1][c.length - 1] / (o.length - 1);
});
const crawlPrice = (url) => __awaiter(void 0, void 0, void 0, function* () {
    // compareImage();
    const sessionId = crypto_1.default.randomUUID();
    fs.mkdirSync(`./assets/${sessionId}/`);
    const aws = yield (0, exports.getProductAWS)(url, sessionId);
    const walmart = yield (0, exports.findProductWalmart)(aws.title, sessionId);
    return { aws, walmart };
});
exports.crawlPrice = crawlPrice;
