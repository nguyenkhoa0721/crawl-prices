"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const helmet_1 = __importDefault(require("helmet"));
const cors_1 = __importDefault(require("cors"));
const morgan_1 = __importDefault(require("morgan"));
const routes_1 = __importDefault(require("./routes"));
const tele_1 = __importDefault(require("./utils/tele"));
//cors
tele_1.default;
const whitelist = process.env.ACCESS_CONTROL_ALLOW_ORIGIN.split(",");
const options = {
    origin: whitelist,
    credentials: true,
};
const app = (0, express_1.default)();
app.use((0, helmet_1.default)());
app.use((0, cors_1.default)(options));
app.use((0, morgan_1.default)("tiny"));
app.use(express_1.default.json());
app.use("/api/v1", routes_1.default);
exports.default = app;
