declare global {
  namespace NodeJS {
    interface ProcessEnv {
      PORT: string;
      ACCESS_CONTROL_ALLOW_ORIGIN: string;
      BOT_TOKEN: string;
    }
  }
}

export {};
